//Includes
//Include TH3
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/action/SpawnNormalAction.h"
#include "game_plugin\Component\SwitchTwoComponent.h"

//Include qsf
#include <qsf/log/LogSystem.h>
#include <qsf/component/base/MetadataComponent.h>

//Include em5
#include <em5/component/vehicle/VehicleComponent.h>
#include "em5\map\MapHelper.h"

//namespace TH3 start
namespace TH3
{

	//Public definitions

	const qsf::NamedIdentifier SpawnNormalAction::ACTION_ID = "TH3::SpawnNormalAction";


	//Public methods

	SpawnNormalAction::SpawnNormalAction() :
		qsf::Action(ACTION_ID)
	{
		// Nothing here
	}

	SpawnNormalAction::~SpawnNormalAction()
	{
		// Nothing here
	}

	void SpawnNormalAction::init(qsf::Entity* entity)
	{
		mEntity = entity;
	}

	//Protected virtual qsf::Action methods

	bool SpawnNormalAction::onStartup()
	{
		return true;
	}

	qsf::action::Result SpawnNormalAction::updateAction(const qsf::Clock& clock)
	{

		SwitchTwoComponent* IC = getEntity().getOrCreateComponent<SwitchTwoComponent>();

		return qsf::action::RESULT_CONTINUE;
	}

} //namespace TH3 end