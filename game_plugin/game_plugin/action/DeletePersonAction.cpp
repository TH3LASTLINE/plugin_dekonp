//Includes
//Include TH3
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/action/DeletePersonAction.h"

//Include qsf
#include <qsf/log/LogSystem.h>
#include <qsf/component/base/MetadataComponent.h>

//Include em5
#include <em5/component/vehicle/VehicleComponent.h>
#include "em5\map\MapHelper.h"

//namespace TH3 start
namespace TH3
{

	//Public definitions

	const qsf::NamedIdentifier DeletePersonAction::ACTION_ID = "TH3::DeletePersonAction";


	//Public methods

	DeletePersonAction::DeletePersonAction() :
		qsf::Action(ACTION_ID)
	{
		// Nothing here
	}

	DeletePersonAction::~DeletePersonAction()
	{
		// Nothing here
	}

	void DeletePersonAction::init(qsf::Entity* entity)
	{
		mEntity = entity;
	}

	//Protected virtual qsf::Action methods

	bool DeletePersonAction::onStartup()
	{
		return true;
	}

	qsf::action::Result DeletePersonAction::updateAction(const qsf::Clock& clock)
	{
		em5::MapHelper::destroyEntity(*mEntity);

		return qsf::action::RESULT_DONE;
	}

} //namespace TH3 end