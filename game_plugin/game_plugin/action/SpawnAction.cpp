//Includes
//Include TH3
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/action/SpawnAction.h"
#include "game_plugin\Component\SwitchComponent.h"

//Include qsf
#include <qsf/log/LogSystem.h>
#include <qsf/component/base/MetadataComponent.h>

//Include em5
#include <em5/component/vehicle/VehicleComponent.h>
#include "em5\map\MapHelper.h"

//namespace TH3 start
namespace TH3
{

	//Public definitions

	const qsf::NamedIdentifier SpawnAction::ACTION_ID = "TH3::SpawnAction";


	//Public methods

	SpawnAction::SpawnAction() :
		qsf::Action(ACTION_ID)
	{
		// Nothing here
	}

	SpawnAction::~SpawnAction()
	{
		// Nothing here
	}

	void SpawnAction::init(qsf::Entity* entity)
	{
		mEntity = entity;
	}

	//Protected virtual qsf::Action methods

	bool SpawnAction::onStartup()
	{
		return true;
	}

	qsf::action::Result SpawnAction::updateAction(const qsf::Clock& clock)
	{

		SwitchComponent* IC = getEntity().getOrCreateComponent<SwitchComponent>();

		return qsf::action::RESULT_CONTINUE;
	}

} //namespace TH3 end