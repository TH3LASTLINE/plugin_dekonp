//Header guard
#pragma once

//Includes
//Inlcude qsf
#include <qsf/logic/action/Action.h>

//Include em5
#include <em5/component/door/DoorComponent.h>

//Forward declarations
namespace qsf
{
	class MeshAnimationChannel;
}

//namespace TH3 start
namespace TH3
{

	//classes
	class SpawnAction : public qsf::Action
	{

		//public definitions
	public:

		static const qsf::NamedIdentifier ACTION_ID;

		//public methods
	public:

		SpawnAction();

		virtual ~SpawnAction();

		void init(qsf::Entity* entity);

		//Protected virtual qsf::Action methods
	protected:

		virtual bool onStartup() override;

		qsf::action::Result SpawnAction::updateAction(const qsf::Clock& clock);

		//private data
	private:

		qsf::Entity* mEntity;

		em5::DoorComponent::DoorType mDoorType;

		bool mOpen;

		//CAMP reflection syste,
		QSF_CAMP_RTTI()

	};

}//namespace TH3 end

 //CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(TH3::SpawnAction)
