// Copyright (C) 2012-2015 Promotion Software GmbH


//[-------------------------------------------------------]
//[ Includes                                              ]
//[-------------------------------------------------------]
#include "game_plugin/PrecompiledHeader.h"
#include "game_plugin/plugin/Plugin.h"

#include <em5/plugin/version/PluginVersion.h>
#include "em5\reflection\CampDefines.h"

#include "qsf_game\command\CommandManager.h"

#include "game_plugin\command\TentDisplacementCommand.h"
#include "game_plugin\command\TentPlacementCommand.h"
#include "game_plugin\command\ChangeToCSACommand.h"
#include "game_plugin\command\ChangeToNormalCommand.h"

#include "game_plugin\action\SpawnAction.h"
#include "game_plugin\action\DeletePersonAction.h"
#include "game_plugin\action\SpawnNormalAction.h"

#include "game_plugin\Component\SwitchTwoComponent.h"
#include "game_plugin\Component\SwitchComponent.h"

//[-------------------------------------------------------]
//[ Namespace                                             ]
//[-------------------------------------------------------]
namespace TH3
{


	//[-------------------------------------------------------]
	//[ Public methods                                        ]
	//[-------------------------------------------------------]
	Plugin::Plugin() :
		qsf::Plugin(new em5::PluginVersion())
	{
		// Nothing to do in here
	}


	//[-------------------------------------------------------]
	//[ Protected virtual qsf::Plugin methods                 ]
	//[-------------------------------------------------------]
	bool Plugin::onInstall()
	{
		try
		{
			// Declare CAMP reflection system classes
			// -> Use Qt's "QT_TR_NOOP()"-macro in order to enable Qt's "lupdate"-program to find the internationalization texts
			
			QSF_START_CAMP_CLASS_EXPORT(TH3::SpawnAction, "", "")
				QSF_CAMP_IS_ACTION
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(TH3::DeletePersonAction, "", "")
				QSF_CAMP_IS_ACTION
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(TH3::SpawnNormalAction, "", "")
				QSF_CAMP_IS_ACTION
			QSF_END_CAMP_CLASS_EXPORT


			QSF_START_CAMP_CLASS_EXPORT(TH3::SwitchComponent, "", "")
				QSF_CAMP_IS_COMPONENT
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(TH3::SwitchTwoComponent, "", "")
				QSF_CAMP_IS_COMPONENT
			QSF_END_CAMP_CLASS_EXPORT


			QSF_START_CAMP_CLASS_EXPORT(TH3::TentPlacementCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(TH3::TentDisplacementCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(TH3::ChangeToCSACommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			QSF_START_CAMP_CLASS_EXPORT(TH3::ChangeToNormalCommand, "", "")
				QSF_GAME_CAMP_IS_COMMAND
			QSF_END_CAMP_CLASS_EXPORT

			// Done
			return true;
		}
		catch (const std::exception& e)
		{
			// Error!
			QSF_ERROR("Failed to install the plugin '" << getName() << "'. Exception caught: " << e.what(), QSF_REACT_NONE);
			return false;
		}
	}

	bool Plugin::onStartup()
	{
		// Nothing to do in here

		qsf::ErrorHandling::setBreakOnAssertForLevel(qsf::ErrorHandling::LEVEL_ERROR, false);
		qsf::ErrorHandling::setBreakOnAssertForLevel(qsf::ErrorHandling::LEVEL_WARNING, false);

		// Done
		return true;
	}

	void Plugin::onShutdown()
	{
		// Nothing to do in here
	}

	void Plugin::onUninstall()
	{
		// Removing classes is not possible within the CAMP reflection system

		// Nothing to do in here
	}


//[-------------------------------------------------------]
//[ Namespace                                             ]
//[-------------------------------------------------------]
} // user
