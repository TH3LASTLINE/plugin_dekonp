//Header guard
#pragma once

//Includes
#include <em5/game/player/Player.h>
//Include qsf
#include "qsf\base\WeakPtr.h"
#include <qsf/map/Map.h>
#include "qsf\debug\DebugDrawProxy.h"
#include "qsf\reflection\CampClass.h"
#include <qsf/selection/SelectionManagerTemplate.h>

//Include qsf_game
#include "qsf_game\command\mode\CommandMode.h"

#include <glm/gtc/quaternion.hpp>

//forward declarations
namespace qsf
{
	class Map;
}

//namespace AMS start
namespace TH3
{

	//Classes

	class TentPlacementCommandmode : public qsf::game::CommandMode
	{

		//public definitions
		public:

			static const uint32 PLUGINABLE_ID;

		//public methods
		public:

			TentPlacementCommandmode();

			virtual ~TentPlacementCommandmode();

			void TentPlacementCommandmode::initialize(qsf::Entity* callerEntity);

		//Public virtual qsf::game::CommandMode methods
		public:

			virtual void startup() override;
			virtual void shutdown() override;
			virtual bool executeCommandContext(qsf::game::CommandContext& commandContext, const EntityIdSet& callerIds) override;
			virtual void updateCommandMode(const qsf::Clock& clock) override;

			glm::quat mCurrentRotation;

		//private methods
		private:

			glm::vec3 getPositionUnderMouse();

		//private definitions
		private:

			static const uint32 ACTION_PRIORITY;

		//private data
		private:

			qsf::Entity* mCommandVehicle;
			glm::vec3 NewPosition;
			qsf::Map* mMap;
			qsf::Entity* mHighlightEntity;
			qsf::Entity* mSpawnpoint;
			qsf::SelectionManagerTemplate<unsigned long long>::IdSet mSelectedIdSet;

		//CAMP reflection system
			QSF_CAMP_RTTI()

	};
}//namespace AMS end

//CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(TH3::TentPlacementCommandmode)