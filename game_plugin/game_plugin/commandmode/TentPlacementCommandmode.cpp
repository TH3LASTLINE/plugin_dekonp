//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\commandmode\TentPlacementCommandmode.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\settings\GameSettingsGroup.h"
#include "em5\command\CommandContext.h"
#include "em5\map\EntityHelper.h"
#include "em5\map\MapHelper.h"
#include <em5/component/vehicle/RoadVehicleComponent.h>
#include <em5/game/Game.h>
#include <em5/game/selection/SelectionManager.h>
#include "em5\action\base\SpawnUnitAction.h"

#include "em5\application\Application.h"
#include "em5/game/groundmap/GroundMaps.h"
#include "em5/game/Game.h"
#include "em5/EM5Helper.h"
#include "em5/component/effect/HighlightComponent.h"
#include "em5/component/effect/GroundSpotComponent.h"
#include "em5\action\base\SpawnUnitAction.h"
#include "em5\game\player\Player.h"
#include "em5\game\player\PlayerManager.h"

//Include qsf
#include "qsf\QsfHelper.h"
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\log\LogSystem.h"
#include "qsf\localization\LocalizationSystem.h"
#include "qsf\settings\SettingsGroupManager.h"
#include <qsf/component/base/TransformComponent.h>
#include <qsf/input/InputSystem.h>
#include <qsf/input/device/MouseDevice.h>

#include "qsf_game\command\mode\CommandMode.h"
#include "qsf_game\command\CommandContext.h"
#include "qsf_game\command\Command.h"
#include <qsf_game/component/base/SelectableComponent.h>
#include "qsf\prototype\Prototype.h"
#include "qsf\prototype\PrototypeManager.h"

//
#include "qsf\map\Map.h"
#include "qsf\map\Entity.h"
#include "qsf\map\EntityHelper.h"
#include <qsf/map/query/RayMapQuery.h>
#include <qsf/map/query/GroundMapQuery.h>
#include <qsf/math/CoordinateSystem.h>
#include <qsf/math/EulerAngles.h>

#include "qsf\map\layer\Layer.h"

#include "qsf\base\WeakPtr.h"
#include "qsf\debug\DebugDrawProxy.h"
#include "qsf\reflection\CampClass.h"

#include "qsf\base\error\ErrorHandling.h"
#include <qsf/message/MessageSystem.h>
#include <qsf/message/MessageConfiguration.h>
#include <em5/plugin/Messages.h>
#include <em5/game/units/OrderInfoManager.h>
#include <em5/game/units/OrderInfo.h>

//namespace AMS start
namespace TH3
{

	//public defintions
	const uint32 TentPlacementCommandmode::PLUGINABLE_ID = qsf::StringHash("AMS::TentPlacementCommandmode");

	//private defintions
	const uint32 TentPlacementCommandmode::ACTION_PRIORITY = em5::action::COMMAND_STD;

	//public methods
	TentPlacementCommandmode::TentPlacementCommandmode() :
		qsf::game::CommandMode(PLUGINABLE_ID),
		mMap(nullptr)
	{

	}

	TentPlacementCommandmode::~TentPlacementCommandmode()
	{

	}

	void TentPlacementCommandmode::initialize(qsf::Entity* callerEntity)
	{
		if (nullptr != callerEntity)
		{
			mCommandVehicle = callerEntity;
			//mCommandVehicle_two = callerEntity;
			mMap = &callerEntity->getMap();
		}
	}

	//public virtual qsf::game::Commandmode methods
	void TentPlacementCommandmode::startup()
	{
		em5::GameSettingsGroup* settings = QSF_SETTINGSGROUP.getById<em5::GameSettingsGroup>("em5::GameSettingsGroup");

		settings->setInputCameraZoomBlocked(true);

		mSelectedIdSet = EM5_GAME.getSelectionManager().getSelectedIdSet();
		EM5_GAME.getSelectionManager().clearSelection();

		mHighlightEntity = mMap->createObjectByLocalPrefabAssetId(qsf::StringHash("em5/prefab/equipment/equipment_firefighter_dekon_tent"));
		mSpawnpoint = mMap->createObjectByLocalPrefabAssetId(qsf::StringHash("em5/prefab/spawnpoint/spawn_dekonp"));
	}

	void TentPlacementCommandmode::shutdown()
	{
		em5::GameSettingsGroup* settings = QSF_SETTINGSGROUP.getById<em5::GameSettingsGroup>("em5::GameSettingsGroup");

		settings->setInputCameraZoomBlocked(false);
		
		mMap->destroyObjectById(mHighlightEntity->getId());
		mHighlightEntity = nullptr;

		mSpawnpoint = nullptr;
	}

	bool TentPlacementCommandmode::executeCommandContext(qsf::game::CommandContext& commandContext, const EntityIdSet& callerIds)
	{
		const em5::OrderInfo* orderInfo = EM5_GAME.getOrderInfoManager().getOrderInfoById(qsf::StringHash("DeKon-P"));		

		qsf::ActionComponent& actionComponent = mSpawnpoint->getOrCreateComponentSafe<qsf::ActionComponent>();

		actionComponent.clearPlan();
		actionComponent.pushAction<em5::SpawnUnitAction>(em5::action::BLOCKING, qsf::action::APPEND_TO_BACK).init(*orderInfo, NewPosition, EM5_PLAYERS.getLocalPlayer()); 

		return true;
	}

	void TentPlacementCommandmode::updateCommandMode(const qsf::Clock& clock)
	{
		NewPosition = getPositionUnderMouse();
		qsf::TransformComponent* transformComponentSpawnpoint = &mSpawnpoint->getComponentSafe<qsf::TransformComponent>();
		qsf::TransformComponent* transformComponent = &mHighlightEntity->getComponentSafe<qsf::TransformComponent>();
		transformComponent->setPosition(NewPosition);
		transformComponentSpawnpoint->setPosition(NewPosition);

		glm::vec3 rotValue(0, 0, 0);

		float pi = (float)(4 * atan(1.0));
		float rot = QSF_INPUT.getMouse().Wheel.getValue();

		if (rot < 0.0)	//links
			rotValue.y = (float)((-10.0*pi) / 180.0);

		else if (rot > 0.0) //rechts
			rotValue.y = (float)((10.0*pi) / 180.0);

		mCurrentRotation = glm::normalize(mCurrentRotation * glm::quat(rotValue));
		transformComponent->setRotation(mCurrentRotation);
		transformComponentSpawnpoint->setRotation(mCurrentRotation);

	}

	//private methods
	glm::vec3 TentPlacementCommandmode::getPositionUnderMouse()
	{
		glm::vec2 mousePosition = QSF_INPUT.getMouse().getPosition();

		qsf::RayMapQueryResponse response = qsf::RayMapQueryResponse(qsf::RayMapQueryResponse::POSITION_RESPONSE);
		qsf::RayMapQuery(*mMap).getFirstHitByRenderWindow(*EM5_APPLICATION.getRenderWindow(), (int)mousePosition.x, (int)mousePosition.y, response);
		glm::vec3 worldPosition = response.position;

		float groundHeight = 0.f;

		qsf::GroundMapQuery(QSF_MAINMAP, em5::GroundMaps::FILTER_DEFAULT).getHeightAt(worldPosition, groundHeight);

		worldPosition.y = groundHeight;

		return worldPosition;
	}

}//namespace AMS end