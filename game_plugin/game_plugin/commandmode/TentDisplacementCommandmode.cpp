//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\commandmode\TentDisplacementCommandmode.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\settings\GameSettingsGroup.h"
#include "em5\command\CommandContext.h"
#include "em5\map\EntityHelper.h"
#include "em5\map\MapHelper.h"
#include <em5/component/vehicle/RoadVehicleComponent.h>
#include <em5/game/Game.h>
#include <em5/game/selection/SelectionManager.h>

#include "em5\application\Application.h"
#include "em5/game/groundmap/GroundMaps.h"
#include "em5/game/Game.h"
#include "em5/EM5Helper.h"
#include "em5/component/effect/HighlightComponent.h"
#include "em5/component/effect/GroundSpotComponent.h"
#include "em5\action\base\SpawnUnitAction.h"
#include "em5\game\player\Player.h"
#include "em5\game\player\PlayerManager.h"

//Include qsf
#include "qsf\QsfHelper.h"
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\log\LogSystem.h"
#include "qsf\localization\LocalizationSystem.h"
#include "qsf\settings\SettingsGroupManager.h"
#include <qsf/component/base/TransformComponent.h>
#include <qsf/input/InputSystem.h>
#include <qsf/input/device/MouseDevice.h>

#include "qsf_game\command\mode\CommandMode.h"
#include "qsf_game\command\CommandContext.h"
#include "qsf_game\command\Command.h"
#include <qsf_game/component/base/SelectableComponent.h>
#include "qsf\prototype\Prototype.h"
#include "qsf\prototype\PrototypeManager.h"

//
#include "qsf\map\Map.h"
#include "qsf\map\Entity.h"
#include "qsf\map\EntityHelper.h"
#include <qsf/map/query/RayMapQuery.h>
#include <qsf/map/query/GroundMapQuery.h>
#include <qsf/math/CoordinateSystem.h>
#include <qsf/math/EulerAngles.h>

#include "qsf\map\layer\Layer.h"

#include "qsf\base\WeakPtr.h"
#include "qsf\debug\DebugDrawProxy.h"
#include "qsf\reflection\CampClass.h"

#include "qsf\base\error\ErrorHandling.h"
#include <qsf/message/MessageSystem.h>
#include <qsf/message/MessageConfiguration.h>
#include <em5/plugin/Messages.h>

//namespace AMS start
namespace TH3
{

	//public defintions
	const uint32 TentDisplacementCommandmode::PLUGINABLE_ID = qsf::StringHash("AMS::TentDisplacementCommandmode");

	//private defintions
	const uint32 TentDisplacementCommandmode::ACTION_PRIORITY = em5::action::COMMAND_STD;

	//public methods
	TentDisplacementCommandmode::TentDisplacementCommandmode() :
		qsf::game::CommandMode(PLUGINABLE_ID),
		mMap(nullptr)
	{

	}

	TentDisplacementCommandmode::~TentDisplacementCommandmode()
	{

	}

	void TentDisplacementCommandmode::initialize(qsf::Entity* callerEntity)
	{
		if (nullptr != callerEntity)
		{
			mCommandVehicle = callerEntity;
			mMap = &callerEntity->getMap();
		}
	}

	//public virtual qsf::game::Commandmode methods
	void TentDisplacementCommandmode::startup()
	{

	}

	void TentDisplacementCommandmode::shutdown()
	{

	}

	bool TentDisplacementCommandmode::executeCommandContext(qsf::game::CommandContext& commandContext, const EntityIdSet& callerIds)
	{
		std::string mTargetEntityStringName = qsf::EntityHelper(*commandContext.mTargetEntity).getName();
		uint64 mTargetEntityId = commandContext.mTargetEntity->getId();

		if (mTargetEntityStringName != "equipment_firefighter_dekon_tent")
			return false;

		mMap->qsf::Map::destroyObjectById(mTargetEntityId);

		return true;
	}

	void TentDisplacementCommandmode::updateCommandMode(const qsf::Clock& clock)
	{

	}

}//namespace AMS end