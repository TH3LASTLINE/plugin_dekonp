//Includes
//Include TH3
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\component\SwitchTwoComponent.h"
#include "game_plugin\action\SpawnNormalAction.h"
#include "game_plugin\action\DeletePersonAction.h"

//Include qsf
#include "qsf\map\Entity.h"
#include "qsf\component\base\TransformComponent.h"
#include "qsf\plugin\QsfJobs.h"
#include "qsf\job\JobArguments.h"
#include "qsf\debug\request\PolygonDebugDrawRequest.h"
#include "qsf\QsfHelper.h"
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\map\Map.h"
#include "qsf\physics\collision\BulletBoxCollisionComponent.h"


#include "em5\action\base\SpawnUnitAction.h"
#include <em5/game/units/OrderInfoManager.h>
#include <em5/game/units/OrderInfo.h>
#include <em5/game/Game.h>
#include "em5\game\player\Player.h"
#include "em5\game\player\PlayerManager.h"
#include "em5\EM5Helper.h"

//Namespace TH3 start
namespace TH3
{

	//Public definitions

	const uint32 SwitchTwoComponent::COMPONENT_ID = qsf::StringHash("TH3::SwitchTwoComponent");

	//public methods

	SwitchTwoComponent::SwitchTwoComponent(qsf::Prototype* prototype) :
		qsf::Component(prototype)
	{

	}

	SwitchTwoComponent::~SwitchTwoComponent()
	{

	}

	//protected virtual em5::Component methods

	bool SwitchTwoComponent::onStartup()
	{
		mUpdateJobProxy.registerAt(qsf::QsfJobs::SIMULATION, boost::bind(&SwitchTwoComponent::updateJob, this, _1));

		return true;
	}

	void SwitchTwoComponent::onShutdown()
	{
		mUpdateJobProxy.unregister();

	}

	//Private methods

	void SwitchTwoComponent::updateJob(const qsf::JobArguments& jobArguments)
	{
		qsf::ActionComponent* AC = getEntity().getComponent<qsf::ActionComponent>();

		if (AC->getCurrentAction() != AC->getAction<SpawnNormalAction>())
			return;

		const em5::OrderInfo* orderInfo = EM5_GAME.getOrderInfoManager().getOrderInfoById(qsf::StringHash("bf_fm"));
		qsf::Entity* mEntity = &getEntity();
		qsf::Map* mMap = &mEntity->getMap();

		qsf::Entity* mSpawnpoint = mMap->createObjectByLocalPrefabAssetId(qsf::StringHash("em5/prefab/spawnpoint/spawn_csa"));

		qsf::TransformComponent* mTransformComponentOne = mEntity->getComponent<qsf::TransformComponent>();
		glm::vec3 mPosition = mTransformComponentOne->getPosition();
		glm::quat mRotation = mTransformComponentOne->getRotation();

		qsf::TransformComponent* mTransformComponentTwo = mSpawnpoint->getComponent<qsf::TransformComponent>();
		mTransformComponentTwo->setPosition(mPosition);
		mTransformComponentTwo->setRotation(mRotation);

		qsf::BulletBoxCollisionComponent* mBBCComponent = mEntity->getComponent<qsf::BulletBoxCollisionComponent>();

		mBBCComponent->setActive(false);

		qsf::ActionComponent* mActionComponentSpawnpoint = &mSpawnpoint->getOrCreateComponentSafe<qsf::ActionComponent>();

		mActionComponentSpawnpoint->pushAction<em5::SpawnUnitAction>(100, qsf::action::APPEND_TO_BACK).init(*orderInfo, mPosition, EM5_PLAYERS.getLocalPlayer());

		AC->clearPlan();
		AC->pushAction<DeletePersonAction>(100, qsf::action::APPEND_TO_BACK).init(mEntity);

		mUpdateJobProxy.unregister();
	}

}//namespace TH3 end