//Header Guard
#pragma once

//Includes
#include "game_plugin\Export.h"

#include "qsf\component\Component.h"
#include "qsf\job\JobProxy.h"
#include "qsf\debug\DebugDrawProxy.h"

// Namespace TH3 start
namespace TH3
{

	//Classes

	class GAMEPLUGIN_API_EXPORT SwitchComponent : public qsf::Component
	{

		//public definitions
	public:

		static const uint32 COMPONENT_ID;

		//public methods
	public:

		SwitchComponent(qsf::Prototype* prototype);

		~SwitchComponent();

		//protected virtual qsf::Component methods
	protected:

		virtual bool onStartup() override;
		virtual void onShutdown() override;

		//private methods
	private:

		void updateJob(const qsf::JobArguments& jobArguments);

		//private data
	private:

		qsf::JobProxy mUpdateJobProxy;

		//CAMP Reflection system
		QSF_CAMP_RTTI()
	};

}//namespace TH3 end

 //CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(TH3::SwitchComponent)
