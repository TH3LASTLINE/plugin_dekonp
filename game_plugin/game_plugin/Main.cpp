// Copyright (C) 2012-2015 Promotion Software GmbH


//[-------------------------------------------------------]
//[ Includes                                              ]
//[-------------------------------------------------------]
#include "game_plugin/PrecompiledHeader.h"


// Is this project built as shared library?
#ifdef _USRDLL

	//[-------------------------------------------------------]
	//[ Includes                                              ]
	//[-------------------------------------------------------]
	#include "game_plugin/Export.h"
	#include "game_plugin/plugin/Plugin.h"


	//[-------------------------------------------------------]
	//[ Global functions                                      ]
	//[-------------------------------------------------------]
	GAMEPLUGIN_FUNCTION_EXPORT qsf::Plugin* CreatePluginInstance_plugin_dekonp()
	{
		return new TH3::Plugin();
	}

#endif
