//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\TentPlacementCommand.h"
#include "game_plugin\commandmode\TentPlacementCommandmode.h"

//Include em5
#include "em5\EM5Helper.h"
#include "em5\application\Application.h"
#include "em5\action\move\MoveAction.h"
#include "em5\action\ActionPriority.h"
#include "em5\game\groundmap\GroundMaps.h"

//Include qsf 
#include "qsf\QsfHelper.h"
#include "qsf\input\InputSystem.h"
#include "qsf\input\device\MouseDevice.h"
#include "qsf\map\query\RayMapQuery.h"
#include "qsf\map\query\GroundMapQuery.h"

//Include qsf_game
#include "qsf_game\QsfGameHelper.h"
#include "qsf_game\command\CommandContext.h"
#include <qsf_game\command\CommandSystem.h>

//namespace AMS start
namespace TH3
{

	int64 timer = 0;
	glm::vec3 NewMousePos = glm::vec3(0, 0, 0);

	//public defintions
	const uint32 TentPlacementCommand::PLUGINABLE_ID = qsf::StringHash("TH3::TentPlacementCommand");

	//private definitions
	const uint32 TentPlacementCommand::ACTION_PRIORITY = 200;

	//public methods
	TentPlacementCommand::TentPlacementCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mPriority = 99;

		mIconSettings.mShowAsGuiButton = true;
		mIconSettings.mButtonIconPath = "dekonp_build";
	}

	bool TentPlacementCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		return true;
	}

	//public virtual em5::Command methods

	bool TentPlacementCommand::checkAvailable()
	{
		return true;
	}

	bool TentPlacementCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriority(caller, ACTION_PRIORITY))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool TentPlacementCommand::checkContext(const qsf::game::CommandContext& context)
	{
		if (!context.mAllowSelfExecution)
			return false;

		return true;
	}

	void TentPlacementCommand::execute(const qsf::game::CommandContext& context)
	{
		TH3::TentPlacementCommandmode *commandmode = new TH3::TentPlacementCommandmode();

		commandmode->initialize(context.mCaller);

		QSFGAME_COMMAND.setCommandMode(*commandmode);
	}

}//namespace AMS end