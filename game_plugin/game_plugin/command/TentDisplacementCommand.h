//Header guard
#pragma once

//Includes
//Include em5
#include "em5\command\Command.h"

//Namespace AMS start
namespace TH3
{

	//Classes
	class TentDisplacementCommand : public em5::Command
	{

		//public definitions
		public:

			static const uint32 PLUGINABLE_ID;

		//public methods
		public:

			explicit TentDisplacementCommand(qsf::game::CommandManager* commandManager);

			bool checkCallerWithoutPriority(qsf::Entity& caller);

		//public virtual em5::command methods
		public:

			virtual bool checkAvailable() override;
			bool checkCaller(qsf::Entity& caller) override;
			virtual bool checkContext(const qsf::game::CommandContext& context) override;
			virtual void execute(const qsf::game::CommandContext& context) override;

		//private definitions
		private:

			static const uint32 ACTION_PRIORITY;

		//CAMP reflection system
			QSF_CAMP_RTTI()

	};

}//namespace AMS end

//CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(TH3::TentDisplacementCommand)