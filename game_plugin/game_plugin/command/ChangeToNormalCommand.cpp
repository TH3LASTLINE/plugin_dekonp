//Includes
//Include TH3
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\ChangeToNormalCommand.h"
#include "game_plugin\action\SpawnNormalAction.h"

//Include em5
#include "em5\action\ActionPriority.h"
#include "em5\EM5Helper.h"
#include "em5\map\EntityHelper.h"
#include "em5\application\Application.h"
#include "em5\action\move\MoveAction.h"
#include "em5\action\ActionPriority.h"
#include "em5\action\base\SpawnUnitAction.h"
#include "em5\game\groundmap\GroundMaps.h"
#include "em5\game\targetpoint\GetEquipmentTargetPointProvider.h"
#include "em5\game\targetpoint\EnterCoDriverDoorTargetPointProvider.h"
#include "em5\action\move\TurnToAction.h"
#include "em5\component\vehicle\VehicleComponent.h"
#include "em5\component\vehicle\RoadVehicleComponent.h"
#include "em5\component\vehicle\HelicopterComponent.h"
#include "em5\base\EquipmentAssets.h"
#include "em5\component\door\DoorComponent.h"
#include <em5/game/units/OrderInfoManager.h>
#include <em5/game/units/OrderInfo.h>
#include "em5\game\player\Player.h"
#include "em5\game\player\PlayerManager.h"
#include <em5/game/Game.h>
#include "em5\event\tutorial\TutorialStep.h"
#include <em5/game/selection/SelectionManager.h>
#include "em5\map\MapHelper.h"

//Include qsf 
#include "qsf\logic\action\ActionComponent.h"
#include "qsf\QsfHelper.h"
#include "qsf\map\query\RayMapQuery.h"
#include "qsf\map\query\GroundMapQuery.h"
#include "qsf\map\query\ComponentMapQuery.h"
#include "qsf\map\EntityHelper.h"
#include "qsf\map\Map.h"
#include "qsf\component\base\MetadataComponent.h"
#include "qsf\component\base\TransformComponent.h"
#include "qsf\physics\collision\BulletBoxCollisionComponent.h"

//Include qsf_ai
#include "qsf_ai\navigation\goal\ReachObjectGoal.h"
#include <qsf_ai/navigation/goal/ReachSinglePointGoal.h>

//Include qsf_logic
#include "qsf_logic\targetpoint\TargetPoint.h"

//Include qsf_game
#include "qsf_game\QsfGameHelper.h"
#include "qsf_game\command\CommandContext.h"
#include <qsf_game\command\CommandSystem.h>
#include "qsf_game\equipment\InventoryComponent.h"
#include "qsf_game\equipment\EquipmentComponent.h"

//namespace TH3 start
namespace TH3
{

	//public defintions
	const uint32 ChangeToNormalCommand::PLUGINABLE_ID = qsf::StringHash("TH3::ChangeToNormalCommand");

	//private definitions
	const uint32 ChangeToNormalCommand::ACTION_PRIORITY = 200;

	//public methods
	ChangeToNormalCommand::ChangeToNormalCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mPriority = 99;

		mIconSettings.mShowAsGuiButton = true;
		mIconSettings.mButtonIconPath = "change_to_normal";
	}

	bool ChangeToNormalCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		return true;
	}

	//public virtual em5::Command methods

	bool ChangeToNormalCommand::checkAvailable()
	{
		return true;
	}

	bool ChangeToNormalCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriority(caller, ACTION_PRIORITY))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool ChangeToNormalCommand::checkContext(const qsf::game::CommandContext& context)
	{
		if (!context.mAllowSelfExecution)
			return false;

		return true;
	}

	void ChangeToNormalCommand::execute(const qsf::game::CommandContext& context)
	{
		qsf::ActionComponent& mActionComponent = getActionComponent(*context.mCaller);
		qsf::Entity* ClosestEntity;

		ClosestEntity = ChangeToNormalCommand::ReturnClosestEntityWithInventory(context.mCaller, "equipment_firedepartment_csa", "em5/prefab/equipment/equipment_firedepartment_csa");

		qsf::TransformComponent* mTC = ClosestEntity->getComponent<qsf::TransformComponent>();
		qsf::Transform mTargetTransform = mTC->getTransform();
		glm::vec3 mPosition = mTargetTransform.getPosition();

		glm::vec3 mChangePosition(5, 5, 0);

		mPosition = mPosition + mChangePosition;

		qsf::logic::TargetPoint targetPoint = qsf::logic::TargetPoint(mPosition, true, 0.f, 0.f, mTargetTransform.getRotation());

		mActionComponent.clearPlan();
		mActionComponent.pushAction<em5::MoveAction>(100, qsf::action::APPEND_TO_BACK).init(new qsf::ai::ReachSinglePointGoal(targetPoint));
		mActionComponent.pushAction<em5::TurnToAction>(100, qsf::action::APPEND_TO_BACK).init(ClosestEntity->getId());
		mActionComponent.pushAction<SpawnNormalAction>(90, qsf::action::APPEND_TO_BACK).init(context.mCaller);

	}

	qsf::Entity* ChangeToNormalCommand::ReturnClosestEntity(qsf::Entity* Doctor, std::vector<qsf::Entity*, std::allocator<qsf::Entity*>> VectorList, float maxrange)
	{
		if (Doctor == nullptr)
			return nullptr;

		if (VectorList.empty())
			return nullptr;

		float Range = -1;
		qsf::Entity* ReturnedUnit = nullptr;

		for (auto Unit : VectorList)
		{
			if (Unit == nullptr)
				continue;

			float currentSquaredDistance = glm::distance2(em5::EntityHelper(*Doctor).getPosition(), em5::EntityHelper(*Unit).getPosition());

			if ((currentSquaredDistance < Range || Range == -1) && maxrange > currentSquaredDistance)
			{
				Range = currentSquaredDistance;
				ReturnedUnit = Unit;
			}
		}
		return ReturnedUnit;
	}

	qsf::Entity* ChangeToNormalCommand::ReturnClosestEntityWithInventory(qsf::Entity* Doctor, std::string Inventory, std::string InventoryFullName)
	{
		qsf::Entity* ClosesetEntity = nullptr;
		em5::RoadVehicleComponent* ClosestVehicle = em5::EntityHelper(*Doctor).getReachableRoadVehicleWithEquipment(Inventory);
		std::vector<qsf::Entity*, std::allocator<qsf::Entity*>> a;

		if (ClosestVehicle != nullptr)
			a.push_back(&ClosestVehicle->getEntity());

		qsf::ComponentCollection::ComponentList<em5::HelicopterComponent> AllHelicopters = qsf::ComponentMapQuery(QSF_MAINMAP).getAllInstances<em5::HelicopterComponent>();

		for (auto Heli : AllHelicopters)
		{
			qsf::Entity* HeliEntity = &Heli->getEntity();

			if (HeliEntity == nullptr)
				continue;
			if (Heli->isFlying())
				continue;
			if (!ChangeToNormalCommand::CheckVehicleForRightEquipment(HeliEntity, InventoryFullName))
				continue;
			if (em5::EntityHelper(*HeliEntity).isSquadVehicleInValidState())
				a.push_back(HeliEntity);

		}

		if (a.empty())
		{
			return nullptr;
		}

		return (ClosesetEntity = ChangeToNormalCommand::ReturnClosestEntity(Doctor, a));
	}

	bool ChangeToNormalCommand::CheckVehicleForRightEquipment(qsf::Entity* Vehicle, std::string Inventory)
	{
		qsf::game::InventoryComponent* IC = Vehicle->getComponent<qsf::game::InventoryComponent>();

		if (IC == nullptr)
			return false;

		for (size_t i = 0; i < IC->InventoryArray.size(); i++)
		{

			if (IC->InventoryArray.get(i) == Inventory)
				return true;

		}

		return false;
	}

}//namespace TH3 end