//Header guard
#pragma once

//Includes
#include "em5\command\Command.h"

//Namespace TH3 start
namespace TH3
{

	//Classes
	class ChangeToCSACommand : public em5::Command
	{

		//public definitions
	public:

		static const uint32 PLUGINABLE_ID;

		//public methods
	public:

		explicit ChangeToCSACommand(qsf::game::CommandManager* commandManager);

		bool checkCallerWithoutPriority(qsf::Entity& caller);

		//Public virtual em5::command methods
	public:

		virtual bool checkAvailable() override;
		bool checkCaller(qsf::Entity& caller) override;
		virtual bool checkContext(const qsf::game::CommandContext& context) override;
		virtual void execute(const qsf::game::CommandContext& context) override;

		static qsf::Entity* ChangeToCSACommand::ReturnClosestEntity(qsf::Entity* Doctor, std::vector<qsf::Entity*, std::allocator<qsf::Entity*>> VectorList, float maxrange = 9999);
		static qsf::Entity* ChangeToCSACommand::ReturnClosestEntityWithInventory(qsf::Entity* Doctor, std::string Inventory, std::string InventoryFullName);
		static bool ChangeToCSACommand::CheckVehicleForRightEquipment(qsf::Entity* Vehicle, std::string Inventory);

		//private definitions
	private:

		static const uint32 ACTION_PRIORITY;

		//CAMP reflection System
		QSF_CAMP_RTTI()
	};

}//Namespace TH3 end

 //CAMP reflection system
QSF_CAMP_TYPE_NONCOPYABLE(TH3::ChangeToCSACommand)