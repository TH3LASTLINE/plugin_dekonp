//Includes
//Include AMS
#include "game_plugin\PrecompiledHeader.h"
#include "game_plugin\command\TentDisplacementCommand.h"
#include "game_plugin\commandmode\TentDisplacementCommandmode.h"

//Include em5
#include "em5\EM5Helper.h"
#include "em5\action\ActionPriority.h"

//Include qsf
#include "qsf\QsfHelper.h"

//Include qsf_game
#include "qsf_game\QsfGameHelper.h"
#include "qsf_game\command\CommandContext.h"
#include <qsf_game\command\CommandSystem.h>

//namespace AMS start
namespace TH3
{

	//public definitions
	const uint32 TentDisplacementCommand::PLUGINABLE_ID = qsf::StringHash("TH3::TentDisplacementCommand");

	//private definitions
	const uint32 TentDisplacementCommand::ACTION_PRIORITY = 200;

	//public methods
	TentDisplacementCommand::TentDisplacementCommand(qsf::game::CommandManager* commandManager) :
		em5::Command(commandManager, PLUGINABLE_ID)
	{
		mPriority = 99;

		mIconSettings.mShowAsGuiButton = true;
		mIconSettings.mButtonIconPath = "dekonp_unbuild";
	}

	bool TentDisplacementCommand::checkCallerWithoutPriority(qsf::Entity& caller)
	{
		return true;
	}

	bool TentDisplacementCommand::checkAvailable()
	{
		return true;
	}

	bool TentDisplacementCommand::checkCaller(qsf::Entity& caller)
	{
		if (!checkCallerActionPriority(caller, ACTION_PRIORITY))
			return false;

		if (!checkCallerWithoutPriority(caller))
			return false;

		return true;
	}

	bool TentDisplacementCommand::checkContext(const qsf::game::CommandContext& context)
	{
		if (!context.mAllowSelfExecution)
			return false;

		return true;
	}

	void TentDisplacementCommand::execute(const qsf::game::CommandContext& context)
	{
		TH3::TentDisplacementCommandmode *commandmode = new TH3::TentDisplacementCommandmode();

		commandmode->initialize(context.mCaller);

		QSFGAME_COMMAND.setCommandMode(*commandmode);
	}

}//namespace AMS end